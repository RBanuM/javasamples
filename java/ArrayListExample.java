
import java.util.*;

public class ArrayListExample
    {
        public static void main(String[] args)
        {
            ArrayList<Integer> al = new ArrayList<>();

            al.add(1000);
            al.add(200);
            al.add(300);

            System.out.println(al);

            //Sorting the arraylist in the defaukt natural sorting order
            Collections.sort(al);

            System.out.println(al);

            //System.out.println("My ArrayList in reverse Order" + al.reverse());

            //iterating through arraylist using foreach method
            for (int myElement: al)
                System.out.println(myElement);

            al.remove(1);
            System.out.println(al);

            al.add(1, 2000);

            //Inserting a set from collection interface
            Integer[] list = {100, 200, 300, 400, 500, 600, 100, 200, 300, 400};
            Set<Integer> set = new HashSet<>(Arrays.asList(list));

            System.out.println(set);
            al.addAll(set);
            System.out.println(al);


        }
    }
