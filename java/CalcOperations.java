public interface CalcOperations {

    public Object add(Object obj1, Object obj2);
    public Object subtract(Object obj1, Object obj2);
    public Object multiply(Object obj1, Object obj2);
    public Object divide(Object obj1, Object obj2);

}
