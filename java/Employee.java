import java.util.ArrayList;
import java.util.List;

public class Employee {

    private int empId;
    private String empName;
    private double salary;

    Employee(int id, String name, double sal)
    {
        this.empId = id;
        this.empName = name;
        this.salary = sal;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public static void main(String[] args)
    {
        Employee emp1 = new Employee(1, "WhoAmI", 1000.0F);
        Employee emp2 = new Employee(2, "GuessMe", 2000.0F);
        Employee emp3 = new Employee(3, "GuessMeNot", 3000.0F);
        Employee emp4 = new Employee(4, "TrustMe", 4000.0F);

        List<Employee> aList = new ArrayList<>();
        aList.add(emp1);
        aList.add(emp2);
        aList.add(emp3);
        aList.add(emp4);


        boolean b = compareSalary(emp1.getSalary(), emp2.getSalary());
        System.out.println("Comparing employees one and two salary " + b);

        b = compareSalary(emp2.getSalary(), emp2.getSalary());
        System.out.println("Comparing employees two and two salary " + b);

        b = compareSalary(emp1.getSalary(), emp4.getSalary());
        System.out.println("Comparing employees one and four salary " + b);

    }

    static boolean compareSalary(double sal1, double sal2)
    {
        if (sal1 - sal2 == 0)
            return true;
        else
            return false;
    }



}
