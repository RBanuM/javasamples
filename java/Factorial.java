public class Factorial {
    public static void main(String[] args) {
        int num = 6;

        int factorial = factorial(num);
        System.out.println("Factorial = " + factorial);
    }

    static int factorial(int num)
    {
        int fact = 1;
        if (num == 0)
            return 1;

        int i = 1;
        while (i <= num)
        {
            fact *= i;
            i++;
        }
        return fact;
    }
}
