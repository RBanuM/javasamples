import java.util.HashMap;
import java.util.Map;

public class HMap {

    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();

        hashMap.put(10, "Lata");
        hashMap.put(20, "Geetha");
        hashMap.put(80, "Seetha");
        hashMap.put(40, "Rama");
        hashMap.put(30, "Pooja");
        hashMap.put(null, "Siri");
        hashMap.put(null, "Sampada");

        System.out.println(hashMap);

        hashMap.remove(20);

        System.out.println(hashMap);

        //Iterate through the hashmap using key, value
        System.out.println("Printing all hash value...");
        for (Map.Entry ent : hashMap.entrySet())
            System.out.println("Key: " + ent.getKey() + " Value: " + ent.getValue());


    }
}
