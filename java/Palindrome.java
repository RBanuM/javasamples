import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {

        /*
        Reading inout from console not required for now
         */
//        System.out.println("Enter a string");
//        Scanner sc = new Scanner(System.in);
//        String s = sc.nextLine();
//        System.out.println("Input String: "+ s);

        String s = "1234523";
        String s1 = "12321";

        isPalindrome(s);
        isPalindrome(s1);
        isPalindrome("malayalam");
        isPalindrome("123321");

    }

    static void isPalindrome(String s)
    {
        int len = s.length();
        int i = 0;
        for (i = 0; i < len/2; i++)
        {
            char left = s.charAt(i);
            char right = s.charAt(len - (i+1));
            if (left != right)
            {
                System.out.println("String " + s + " is not palindrome");
                break;
            }
        }

        if (i >= len/2)
            System.out.println("String " + s + " is a palindrome");
    }
}
