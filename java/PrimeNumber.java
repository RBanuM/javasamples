public class PrimeNumber {
    public static void main(String[] args)
    {
        int i = 0;
        //System.out.println("Enter a number");
        int num = 77;
        isPrime(num);
        isPrime(100);
        isPrime(5);
        isPrime(9);
    }

    static void isPrime(int n)
    {
        int i = 2;

        for (i = 2; i < n/2; i ++)
        {
            if (n%i == 0)
                break;
        }
        if (i < (n /2))
            System.out.println("Number is not prime");
        else
            System.out.println("Number is prime");

    }
}

