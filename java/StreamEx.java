import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamEx {

    public static void main(String[] args)
    {

        List<Integer> integers = Arrays.asList(1,2,3,4,5);

        int result = 0;
        for (Integer integer: integers) {
            if (integer % 2 == 0) {
                result += integer;
            }
        }

        System.out.println(result);

        //Practice code for using Predicate
        Predicate<Integer> predicate = new Predicate<Integer>() {

            @Override
            public boolean test(Integer integer) {
                return integer %2 == 0;
        }
        };

        System.out.println(integers.stream().filter(predicate) );

        System.out.println(integers.stream().filter(i -> i % 2 == 0).reduce(0, (i, res) -> i + res));

        System.out.println(integers.stream().map(i -> i * 2). collect(Collectors.toList()));

        System.out.println(integers);


    }
}
