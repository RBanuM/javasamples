import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamEx1 {

        public static void main(String[] args)
        {

            List<String> str = Arrays.asList("abc", "","bcd", "defg", "jk");


            System.out.println(str);


            System.out.println(str.stream().filter(i -> i.length() > 2).collect(Collectors.toList()));


            List<String> str1 = Arrays.asList("USA", "Japan", "France", "Belgium");
            System.out.println(str1.stream().map(i -> i.toUpperCase()).collect(Collectors.joining(",")));

            List<Integer> integers = Arrays.asList(9,10,3,4,7,3,4);
            List<Integer> unique = integers.stream().distinct().collect(Collectors.toList());
            System.out.println(unique.stream().map(i->i*i).collect(Collectors.toList()));


            List<Integer> intCount = Arrays.asList(1,2,2,4,2,5);
            //Count the number of 2's in the input
            Long cnt = intCount.stream().filter(i -> i==2).count();
            System.out.println("Count of 2's = "+ cnt);
            //System.out.println(cnt);(i->i == 2).count().collect(Collectors.toList());
            ArrayList items = new ArrayList();

            //count 2'2 and 4's
            cnt = intCount.stream().filter(i -> i==2 || i == 4).count();
            System.out.println("Count of 2's and 4's= "+ cnt);
            //items.stream().filter(o -> o.brandname == "asda" && o.itemType == "f")
        }
    }

