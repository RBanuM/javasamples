import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;

public class TSet {
    public static void main(String[] args) {
        TreeSet<Integer> ts = new TreeSet<>(new MyComparator());
        //TreeSet<Integer> ts = new TreeSet<Integer>();
        ts.add(57);
        ts.add(300);
        ts.add(200);

        //ts.add(null); //TreeSet does not allow null, throws NULL Pointer Exception
        ts.add(23);
        //Adding duplicates intentionally
        ts.add(300);
        Integer[] arr = {11, 33, 55, 99,222, 333, 444, 555, 666};
        ts.addAll(Arrays.asList(arr));
        System.out.println(ts);

    }
}

/* Instead of using the default natural sorting order
** we can implement the Comparator interface to sort the elements in descending order
 */

class MyComparator implements Comparator {

        public int compare(Object obj1, Object obj2)
        {
            Integer i1 = (Integer) obj1;
            Integer i2 = (Integer) obj2;
            return -(Integer.compare(i1, i2));

        }
    }