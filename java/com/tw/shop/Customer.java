package com.tw.shop;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Customer implements ICart{

    HashMap<String, String> cart = new HashMap<String, String>();

    public void initialize() {
        WebsiteOwner siteOwner = new WebsiteOwner();
        HashMap<String, ProdDetails> handleToCalatog = siteOwner.handleToProductCatalog();

    }

    @Override
    public void addToCart(String userId, String prodId) {
        System.out.println("ADDING TO CART " + userId + " " + prodId);
        cart.put("userid", prodId);

    }

    @Override
    public void deleteFromCart(String userId, String prodId) {
        //ASSUMPTION: userid -- dummy for now
        System.out.println("DELETING FROM CART " + userId + " " + prodId);
        Iterator<Map.Entry<String, String>> entryIt = cart.entrySet().iterator();
        while (entryIt.hasNext()) {
            Map.Entry<String, String> entry  = entryIt.next();
            if (prodId.equals((String)entry.getValue())) {
                System.out.println("KEY: " + entry.getKey() + " VALUE : " + entry.getValue());
                entryIt.remove();
            }
        }
        //cart.remove("userid", prodId);
    }

    @Override
    public void viewAll(String userId) {
        System.out.println("CART ENTRIES");
        Iterator<Map.Entry<String, String>> entryIt = cart.entrySet().iterator();
        while (entryIt.hasNext()) {
            Map.Entry<String, String> entry  = entryIt.next();
            System.out.println("KEY: " + entry.getKey() + " VALUE : " + entry.getValue());
            }
    }

    @Override
    public void emptyCart(String userId) {
        System.out.println("--- EMPTY CART ENTRIES ---");
        Iterator<Map.Entry<String, String>> entryIt = cart.entrySet().iterator();
        while (entryIt.hasNext()) {
            Map.Entry<String, String> entry  = entryIt.next();
            System.out.println("KEY: " + entry.getKey() + " VALUE : " + entry.getValue());
            entryIt.remove();
        }
    }
}
