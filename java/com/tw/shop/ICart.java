package com.tw.shop;

public interface ICart {
    public void addToCart(String userId, String prodId);
    public void deleteFromCart(String userId, String prodId);
    public void viewAll(String userId);
    public void emptyCart(String userId);
}
