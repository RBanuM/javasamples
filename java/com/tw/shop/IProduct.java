package com.tw.shop;

import java.util.HashMap;

/*
** Many more APIs can be added
 */
public interface IProduct {

    public HashMap<String, ProdDetails> handleToProductCatalog();
    public void addProduct(String bName, String prodCategory, String prodName, int uniqueId);
    public int generateProductId();

    public void listAllProducts();

    public void deleteProduct(String bName, String prodCategory, String prodName, int id);
    public void deleteProductByProductID(String prodId);

    public void deleteBrand(String bName);
    public boolean isBrandAvailable(String bName);
    /*

    //Not Yet Implemented
    public boolean isProductCategoryAvailable();

     */
}
