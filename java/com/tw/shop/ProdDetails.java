package com.tw.shop;

/*
 * itemCount and itemSIze can be declared long to accomodate more items
 */
public class ProdDetails {

    private String brandName;
    private String prodCategory;
    private String prodName;
    private String itemName;
    private int itemCount;
    private int itemSize;

    public ProdDetails(String bName, String prodCategory, String prodName,
                     String itemName, int itemCount, int itemSize){
        this.brandName =  bName;
        this.prodCategory = prodCategory;
        this.prodName = prodName;
        this.itemName = itemName;
        this.itemCount = itemCount;
        this.itemSize = itemSize;
    }

    public ProdDetails(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProdCategory() {
        return prodCategory;
    }

    public void setProdCategory(String prodCategory) {
        this.prodCategory = prodCategory;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public int getItemSize() {
        return itemSize;
    }

    public void setItemSize(int itemSize) {
        this.itemSize = itemSize;
    }


}
