

/*
** ASSUMPTIONS:
** (1) EACH OF THE BRAND NAME CAN BE AN ENUMERATION
** (2) HARDCODED ITEM SIZE, ITEM COUNT AND ITEM SIZE
** (3) BOUNDARY CONDITIONS NOT HANDLED -- SHORTAGE OF TIME
** (4) USERID HARDCODED ON CUSTOMERID
** (5) MORE METHODS CAN BE ADDED TO THE INTERFACE
 */

package com.tw.shop;

import java.util.*;

public class WebsiteOwner implements IProduct{


    private static int prodCount = 0;
    private HashMap<String, ProdDetails>  productCatalog = new HashMap<String, ProdDetails>();

    public HashMap<String, ProdDetails> handleToProductCatalog()
    {
        return productCatalog;
    }

    public static void main(String[] args) {

        //HashMap<String, ArrayList<ProdDetails> >  productCatalog = new HashMap<String, java.util.ArrayList<ProdDetails>>();

        WebsiteOwner iProd = new WebsiteOwner();
        Customer cust = new Customer();
        cust.initialize();

        //ELECTRONICS
        iProd.addProduct("SAMSUNG", "ELECTRONICS", "TV32INCH", iProd.generateProductId());
        iProd.addProduct("SAMSUNG", "ELECTRONICS", "TV42INCH", iProd.generateProductId());
        iProd.addProduct("SAMSUNG", "ELECTRONICS", "MOBILEON8", iProd.generateProductId());
        iProd.addProduct("SAMSUNG", "ELECTRONICS", "REFRIDGERATORDOUBLEDOOR", iProd.generateProductId());

        //PUMA
        iProd.addProduct("PUMA", "cloth", "FormalShirt", iProd.generateProductId());
        iProd.addProduct("PUMA", "cloth", "TShirt", iProd.generateProductId());
        iProd.addProduct("PUMA", "cloth", "FormalPant", iProd.generateProductId());
        iProd.addProduct("PUMA", "cloth", "Jeans", iProd.generateProductId());
        iProd.addProduct("PUMA", "shoes", "type1", iProd.generateProductId());
        iProd.addProduct("PUMA", "shoes", "type2", iProd.generateProductId());
        iProd.addProduct("PUMA", "sportsshoes", "type1", iProd.generateProductId());
        iProd.addProduct("PUMA", "sportsshoes", "type2", iProd.generateProductId());

        //VAN HEUSEN
        iProd.addProduct("VANHEUSEN", "cloth", "FormalShirt", iProd.generateProductId());
        iProd.addProduct("VANHEUSEN", "cloth", "TShirt", iProd.generateProductId());
        iProd.addProduct("VANHEUSEN", "cloth", "FormalPant", iProd.generateProductId());
        iProd.addProduct("VANHEUSEN", "cloth", "Jeans", iProd.generateProductId());

        //UNITED COLORS OF BENETTTON
        iProd.addProduct("UNITEDCOLORSOFBENETTON", "cloth", "FormalShirt", iProd.generateProductId());
        iProd.addProduct("UNITEDCOLORSOFBENETTON", "cloth", "TShirt", iProd.generateProductId());
        iProd.addProduct("UNITEDCOLORSOFBENETTON", "cloth", "FormalPant", iProd.generateProductId());
        iProd.addProduct("UNITEDCOLORSOFBENETTON", "cloth", "Jeans", iProd.generateProductId());

        iProd.listAllProducts();
        iProd.isBrandAvailable("PUMA");
        iProd.isBrandAvailable("UNITEDCOLORSOFBENETTON");
        iProd.deleteProductByProductID("PUMAclothFormalPant7");
        iProd.deleteBrand("PUMA");
        iProd.listAllProducts();

        //JUST For TESTING CLIENT -- CUSTOMER CART OPERATIONS
        System.out.println("\n\nCLIENT CART OPERATIONS\n\n");
        Customer customer = new Customer();
        customer.addToCart("userid", "UNITEDCOLORSOFBENETTONclothFormalPant19");
        customer.addToCart("userid", "SAMSUNGELECTRONICSTV42INCH2");
        customer.addToCart("userid", "SAMSUNGELECTRONICSREFRIDGERATORDOUBLEDOOR4");
        customer.addToCart("userid", "VANHEUSENclothJeans16");
        customer.addToCart("userid", "SAMSUNGELECTRONICSMOBILEON83");
        customer.addToCart("userid", "UNITEDCOLORSOFBENETTONclothFormalShirt17");

        customer.viewAll("userid");
        customer.deleteFromCart("userid", "SAMSUNGELECTRONICSMOBILEON83");

        customer.viewAll("userid");
        customer.emptyCart("userid");

        System.out.println("NOTHING SHOULD GET PRINTED AFTER THIS STATEMENT");
        customer.viewAll("userid");

    }

    @Override
    public void addProduct(String bName, String prodCategory, String prodName, int uniqueId)
    {
        String myUniqueProductId = bName + prodCategory + prodName + uniqueId;
        //System.out.println("MY PRODUCT ID : " + myUniqueProductId);

        ProdDetails prodDetailsEntry = new ProdDetails(bName, prodCategory, prodName, "XXX", 200, 100);

        productCatalog.put(myUniqueProductId, prodDetailsEntry);
    }

    @Override
    public int generateProductId()
    {
        return ++prodCount;
    }

    @Override
    public void listAllProducts()
    {
        System.out.println("\n******* ALL THE PRODUCTS IN THE SYSTEM AS STORED ******\n");
        for(Map.Entry entry: productCatalog.entrySet())
        {
            ProdDetails details = (ProdDetails) entry.getValue();
            System.out.println("PRODUCT ID: " + entry.getKey() + " PRODUCT DETAILS: " + details.getBrandName() + " " +
                    details.getProdCategory() + " " + details.getProdName() + " " + details.getItemName() + " " + details.getItemCount() +
                    " " + details.getItemSize());
        }
    }

    @Override
    public void deleteProduct(String bName, String prodCategory, String prodName, int id)
    {
        String myUniqueProductId = bName + prodCategory + prodName + id;
        System.out.println("MY PRODUCT ID UP FOR DELETION: " + myUniqueProductId);
        productCatalog.remove(myUniqueProductId);
        deleteProductByProductID(myUniqueProductId);
    }

    @Override
    public void deleteProductByProductID(String prodId)
    {
        if (productCatalog.containsKey(prodId)) {
            productCatalog.remove(prodId);
            System.out.println("PRODUCT DELETED FROM CATALOG!!!" + prodId);
        }
        else
        {
            System.out.println("PRODUCT DOES NOT EXIST!!!");
        }
    }

    @Override
    public void deleteBrand(String bName){

        System.out.println("\n******* DELETE THE ENTIRE BRAND ******\n");
        //Iterator<String> it = productCatalog.keySet().iterator();
        Iterator<Map.Entry<String, ProdDetails>> entryIt = productCatalog.entrySet().iterator();
        while (entryIt.hasNext())
        {
            Map.Entry<String, ProdDetails> entry = entryIt.next();
            String key = entry.getKey();
            ProdDetails details = (ProdDetails) entry.getValue();
            System.out.println("SEARCHING FOR BRANDNAME  " + details.getBrandName());
            if (bName.equals(details.getBrandName())) {
                System.out.println("DELETING ITEM WITH DETAILS  " + bName);
                System.out.println("PRODUCT ID: " + entry.getKey() + " PRODUCT DETAILS: " + details.getBrandName() + " " +
                        details.getProdCategory() + " " + details.getProdName() + " " + details.getItemName() + " " + details.getItemCount() +
                        " " + details.getItemSize());
                entryIt.remove();
            }
        }
    }

    @Override
    public boolean isBrandAvailable(String bName)
    {
        System.out.println("\n******* SEARCHING THE ENTIRE BRAND ******\n");
        for(Map.Entry entry: productCatalog.entrySet())
        {
            ProdDetails details = (ProdDetails) entry.getValue();
            if (bName.equals(details.getBrandName())) {
                System.out.println("--- BRAND NAME AVAILABLE --- ");
                return true;
            }
        }
        return false;
    }

}
